#!/bin/bash

docker run -it --rm \
    -v `pwd`:/proj \
    gcc_cmake_gtest:latest \
    sh -c "cd /proj \
        && cmake -H. -BbuildTest -G\"Unix Makefiles\" -DCMAKE_CXX_COMPILER=g++ -DCMAKE_C_COMPILER=gcc \
        && cd buildTest \
        && make coverage"

docker run -it --rm \
    -v `pwd`:/proj \
    gcc_cmake_gtest:latest \
    sh -c "cd /proj \
        && cmake -H. -BbuildQemu -G\"Unix Makefiles\" -DTARGET_ARM=1 -DCMAKE_TOOLCHAIN_FILE=ext/toolchain/gcc_arm_stm32f4_qemu.txt -Dtools_QEMU="" -Dtools_GCC="" \
        && cd buildQemu \
        && make platform_hw_stm32f4_test_qemu"
