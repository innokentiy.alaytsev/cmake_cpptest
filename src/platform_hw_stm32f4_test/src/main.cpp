#include <cstdlib>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <stdio.h>

#include "STM32F4_Discovery.h"
#include "Tests.h"
#include <HW/Driver/GPIO_stm32f4.h>
#include <HW/Driver/UART_stm32f4.h>
#include <HW/Util/Trace.h>
#include <Util/Time.h>
#include <Util/Timing.h>

using namespace ::Platform::Driver;
using namespace ::Platform::Util;
using namespace ::Platform::Timing;

HardwareSTM32F4Discovery* HARDWARE = HardwareSTM32F4Discovery::Instance();


void WaitSomeTicks(){
  Timer timerTick;
  Timer timer;
  timer.Start(1000);
  float last = 0;
  while(!timer.Expired()){
    if(!timerTick.Started() || timerTick.Expired()){
      timerTick.Start(100);
      HARDWARE->ledGreen->Set(!HARDWARE->ledGreen->Value());

      float uptime = Time::Instance()->Uptime();
			Platform::Semihost::Out() << "World " << (int)(uptime * 1000) << "\n";
	  }
  }
}





int main(int argc, char** argv){
  {
    Platform::Semihost::Out() << "---------- Test Execution details ----------" << std::endl;
    Platform::Semihost::Out() << "Running in Path: " << std::endl;
    Platform::Semihost::SysExecute("pwd", 3);
    Platform::Semihost::Out() << "---------- Test Execution details .. done! ----------" << std::endl;
    
    auto time = Time::Instance();
    time->Start(1000);

    // Start Systick
    Time::Instance()->Start(1000);

    Platform::Semihost::Out() << "---------- Do some Ticks ----------" << std::endl;
    WaitSomeTicks();
    Platform::Semihost::Out() << "---------- Do some Ticks .. done! ----------" << std::endl << std::endl;
    
    Platform::Semihost::Out() << "---------- Do Tests ----------" << std::endl;
    Tests::Instance()->Run();
    Platform::Semihost::Out() << "---------- Do Tests .. done! ----------" << std::endl;
  }
  //_exit(0);
  Platform::Semihost::ExitOK();
  return 0;
}