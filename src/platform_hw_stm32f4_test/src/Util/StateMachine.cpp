#include <iostream>
#include <Util/StateMachine.h>
#include "Tests.h"

using namespace ::Platform::Util;

struct TestStateSharedData
{
		TestStateSharedData(){}

		StateMachine<TestStateSharedData>::State*			hello;
		StateMachine<TestStateSharedData>::State*			world;
};



class TestStateHello : public StateMachine<TestStateSharedData>::State
{
	typedef StateMachine<TestStateSharedData>::State Inherited;

	public:
		virtual void OnEnter(TestStateSharedData* sData){
			Inherited::OnEnter(sData);
		}

		virtual Inherited* OnProcess(){
			return Inherited::GetSData()->world;
		}

		virtual void OnExit(){
		}
};


class TestStateWorld : public StateMachine<TestStateSharedData>::State
{
	typedef StateMachine<TestStateSharedData>::State Inherited;

	public:
		virtual void OnEnter(TestStateSharedData* sData){
			Inherited::OnEnter(sData);
		}

		virtual Inherited* OnProcess(){
			return Inherited::GetSData()->hello;
		}

		virtual void OnExit(){
		}
};



class TestStateBase : public StateMachine<TestStateSharedData>
{
	public:
		TestStateBase(){
			sData = new TestStateSharedData;
			sData->hello = new TestStateHello;
			sData->world = new TestStateWorld;

			SetInitialState(sData->hello);
			SetSharedData(sData);
		}

		virtual ~TestStateBase(){
			delete sData->hello;
			delete sData->world;
			delete sData;
		}

		TestStateSharedData* sData;
};


class TEST_TestState : public Test::Suite
{
public:
	TEST_TestState()
	{
    TEST_ADD(TEST_TestState::Initialisation)
    TEST_ADD(TEST_TestState::Step1)
    TEST_ADD(TEST_TestState::Step2)
	}
	
private:
	void Initialisation() {
		TEST_ASSERT_EQUALS(m_testStateBase.GetCurrentState(), NULL);
    m_testStateBase.Execute();
		TEST_ASSERT_EQUALS(m_testStateBase.GetCurrentState(), m_testStateBase.sData->hello);
  }
	
  void Step1() {
    m_testStateBase.Execute();
		TEST_ASSERT_EQUALS(m_testStateBase.GetCurrentState(), m_testStateBase.sData->world);
  }
	
  void Step2() {
    m_testStateBase.Execute();
		TEST_ASSERT_EQUALS(m_testStateBase.GetCurrentState(), m_testStateBase.sData->hello);
  }
	
  TestStateBase										m_testStateBase;
};

static TestAdder<TEST_TestState> test_TestState;