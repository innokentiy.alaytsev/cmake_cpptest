#pragma once

#include "Error.h"

namespace Platform
{
  namespace Util
  {
    template<typename T>
    class List
    {
      public:
        class Iterator;

        class ListItem
        {
          friend List;
          friend Iterator;

          T           m_item;
          ListItem*   m_next;

          public:
            ListItem(T item):
              m_item(item),
              m_next(nullptr)
            {}

            T& GetItem(){
              return m_item;
            }

            ListItem* Next(){
              return m_next;
            }
        };

        class Iterator
        {
          friend List;

          typedef Iterator self_type;
          ListItem*     m_currentListItem;

          public:
            Iterator(ListItem *item) :
              m_currentListItem(item)
            {}

            T& operator *(){
              return m_currentListItem->GetItem();
            }

            self_type operator ++(){
              if(m_currentListItem != nullptr)
                m_currentListItem = m_currentListItem->Next();
              return *this;
            }

            self_type operator ++(int junk){
              self_type t = *this;
              if(m_currentListItem != nullptr)
                m_currentListItem = m_currentListItem->Next();
              return t;
            }

            bool operator ==(const self_type& iter){
              return iter.m_currentListItem == m_currentListItem;
            }

            bool operator !=(const self_type& iter){
              return iter.m_currentListItem != m_currentListItem;
            }
            
            const T operator->() {
              return m_currentListItem->GetItem();
            }
        };

        List() :
          m_head(nullptr),
          m_last(nullptr),
          m_size(0)
        {}

        Iterator Begin(){
          Iterator iter(m_head);
          return iter;
        }

        Iterator End(){
          Iterator iter(nullptr);
          return iter;
        }

        unsigned int Size(){
          return m_size; 
        }

        void Add(T item){
          ListItem* lItem = new ListItem(item);
          if(m_head == nullptr){
            m_head = lItem;
            m_last = m_head;
          }else{
            ListItem* last = m_head;
            while(last->Next() != nullptr){
              last = last->Next();
            }
            last->m_next = lItem;
            m_last = lItem;
          }
          m_size++;
        }

        void Remove(Iterator& itr){
          if(Size() <= 0)
            return;

          ListItem* item = m_head;

          while(item->Next() != itr.m_currentListItem){
            item = item->Next();
          }

          ListItem* itemToBeRemoved = item->m_next;
          item->m_next = itemToBeRemoved->m_next;

          delete itemToBeRemoved;
        }

      private:
        ListItem*   m_head;
        ListItem*   m_last;
        unsigned int m_size;
    };
  }
}