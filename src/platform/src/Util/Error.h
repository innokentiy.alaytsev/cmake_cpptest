#pragma once

#include <exception>

namespace Platform
{
  class Exception : public std::exception
  {
    public:
      Exception(
          const char*const msg,
          const char* file,
          int line,
          const char* func,
          const char* info = "") :
        std::exception(),
        m_info (msg),
        m_file (file),
        m_line (line),
        m_func (func)
      {
      }

      virtual const char * what () const throw ()
      {
        return m_info;
      }

    private:
      const char*             m_info;
      const char*             m_file;
      int                     m_line;
      const char*             m_func;
  };
}

#define DOEXCEPT(__msg) \
{ \
  throw Platform::Exception((__msg), __FILE__, __LINE__, ""); \
}

