#pragma once

#include <Util/Error.h>
#include <Util/Executable.h>


namespace Platform
{
  namespace Util
  {
    template<typename SharedData>
    class StateMachine : public Executable
    {
      public:
        class State
        {
          public:

            void Enter(SharedData* sData)
            {
              OnEnter(sData);
            }
            
            State* Process()
            {
              return OnProcess();
            }
            
            void Exit()
            {
              OnExit();
            }
            
          protected:
            virtual void OnEnter(SharedData* sData){
              if(sData != nullptr)
              {
                m_sData = sData;
              }
            }

            virtual State* OnProcess() = 0;

            virtual void OnExit() = 0;

            SharedData* GetSData(){
              return m_sData;
            }

          private:
            SharedData*         m_sData;
        };

        StateMachine():
          m_nextState(nullptr),
          m_currentState(nullptr),
          m_sData(nullptr)
        {
        }
      
        void SetInitialState(State* state){
          m_nextState = state;
        }
      
        void SetSharedData(SharedData* sData){
          m_sData = sData;
        }

        void OnExecute(){
          if(m_sData == nullptr){
            DOEXCEPT("Statemachine not initialized. shared Data is nullptr")
          }
          if(m_nextState == nullptr){
            DOEXCEPT("Statemachine not initialized. state is nullptr")
          }

          if(m_currentState != m_nextState)
          {
            m_currentState = m_nextState;
            m_currentState->Enter(m_sData);
          }

          m_nextState = m_currentState->Process();
          
          if(m_nextState == nullptr){
            DOEXCEPT("next state is nullptr")
          }
          
          if(m_currentState != m_nextState)
          {
            m_currentState->Exit();
          }
        }

        State* GetCurrentState(){
          return m_currentState;
        }

      private:
        State*            m_currentState;
        State*            m_nextState;
        SharedData*         m_sData;
    };
  }
}

