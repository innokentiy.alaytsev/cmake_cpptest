#include "Time.h"

namespace Platform 
{
  namespace Util
  {
    TimeBase::TimeBase():
      m_ticks(0)
    {
      if(Time::Instance() != nullptr){
        DOEXCEPT("There's already a Time running");
      }
      Time::Init(this);
    }

    TimeBase::~TimeBase()
    {
      Time::Reset();
    }

    
    float TimeBase::Uptime()
    {
      return m_ticks / m_tickFreq;
    }
    
    unsigned long TimeBase::Uptime_ms()
    {
      return m_ticks / m_tickFreq * 1000;
    }
    
    void TimeBase::OnTick(){
      m_ticks++;
      for(typename List<SysTickHook>::Iterator iter = m_hooks.Begin();
          iter != m_hooks.End();
          iter++){
        (*iter)();
      }
    }

    void TimeBase::AddHook(SysTickHook hook){
      m_hooks.Add(hook);
    }
  }
}
