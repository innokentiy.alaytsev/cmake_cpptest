#pragma once

#include <Util/NamedObject.h>

namespace Platform
{
  namespace Util
  {

    class Task : public NamedObject
    {
      public:
        enum Priority{
          IDLE,
          IDLE_ABOVE,
          MID,
          MID_ABOVE,
          HIGH
        };

        Task(
              const std::string& name,
              unsigned int stacksize = 512,
              Priority priority = IDLE) :
              NamedObject(name)
            {}

        virtual void Run() final {
          OnRun();
        }

      protected:
        virtual void OnRun() = 0;
    };
  }
}
