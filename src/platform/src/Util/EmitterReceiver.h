#pragma once

#include "Error.h"
#include <string.h>
#include <Util/List.h>
namespace Platform
{
  namespace Util
  {
    class Emitter
    {};

    class Receiver
    {
      public:
        virtual void LinkBy(Emitter& e) final {
          OnLinkBy(e);
        }
        
        virtual void UnLinkedBy(Emitter& e) final {
          UnLinkedBy(e);
        }

      protected:
        virtual void OnLinkBy(Emitter& e){}
        virtual void OnUnLinked(Emitter& e){}
    };



    template<typename Data>
    class DataReceiver : public Receiver
    {
      public:
        virtual bool Receive(const Data& data) final {
          return OnReceive(data);
        }

      protected:
        virtual bool OnReceive(const Data& data) = 0;
    };



    template<typename Data>
    class DataReceiverStore : public DataReceiver<Data>
    {
      Data m_data;

      public:
        DataReceiverStore(Data default_val) :
          m_data(default_val)
        {}

        Data operator*(void) const {
          return m_data;
        }

      protected:
        virtual bool OnReceive(const Data& data){
          m_data = data;
          return true;
        }
    };



    template <typename Data>
    class DataEmitter : public Emitter
    {
      List<DataReceiver<Data>*> m_receiverList;

      public:

        DataEmitter()
        {}

        virtual bool Emit(const Data& data) final {
          return OnEmit(data);
        }

        virtual bool OnEmit(const Data& data){
          bool result = true;
          for(typename List<DataReceiver<Data>*>::Iterator iter = m_receiverList.Begin();
              iter != m_receiverList.End();
              iter++){
            result &= iter->Receive(data);
          }
          return result;
        }

        virtual void Link(DataReceiver<Data>* r) final {
          if(r == NULL){
            DOEXCEPT("Link an NULL Receiver");
          }
          m_receiverList.Add(r);
          r->LinkBy(*this);
        }

        virtual void UnLink() final {
          for(typename List<DataReceiver<Data>*>::Iterator iter = m_receiverList.Begin();
              iter != m_receiverList.End();
              iter++){
            iter->UnLinkedBy(*this);
          }
        }
    };

    template<typename T1, typename T2 = T1>
    class DataEmitterReceiver : public DataEmitter<T1>, public DataReceiver<T2>
    {

    };
  }
}