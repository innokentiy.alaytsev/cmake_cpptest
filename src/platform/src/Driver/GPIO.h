#pragma once

#include <Driver/Driver.h>
#include <Util/EmitterReceiver.h>
#include <Util/Error.h>
#include <Util/Executable.h>

namespace Platform {
  namespace Driver {
    class GPIO : 
        public Driver,
        public ::Platform::Util::Executable,
        public ::Platform::Util::DataEmitterReceiver<bool> {
      
      public:

        struct Pin{
          unsigned int port;
          unsigned int pin;
        };

        enum Mode{
          INPUT,
          OUTPUT,
          ALT_FUNC,
          ANALOG
        };

        GPIO(const char * const name, Pin& pin):
          Driver(name),
          m_pin(pin)
        {}

        virtual bool SetMode(Mode mode) = 0;

        virtual bool Value(){
          DOEXCEPT("not implemented")
        };

        virtual void Set(const bool state){
          DOEXCEPT("not implemented")
        };

      protected:
        virtual bool OnReceive(const bool& data){
          Set(data);
          return true;
        }

        virtual void OnExecute(){
          DataEmitter::OnEmit(Value());
        }

        Pin&       m_pin;
    };
  };
};