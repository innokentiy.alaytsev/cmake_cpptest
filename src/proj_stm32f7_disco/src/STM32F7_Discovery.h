#pragma once

#include <HW/Driver/GPIO_stm32f7.h>
#include <HW/Util/Time_stm32f7.h>

class HardwareSTM32F7Discovery
{
  typedef ::Platform::Driver::GPIO_STM32F7 GPIO;
  public:
    GPIO* ledGreen;
    GPIO* ledOrange;
    GPIO* ledRed;
    GPIO* ledBlue;
    GPIO* btnUser;
    
    GPIO::Pin ledGreenPin;
    GPIO::Pin ledOrangePin;
    GPIO::Pin ledRedPin;
    GPIO::Pin ledBluePin;
    GPIO::Pin btnUserPin;
    
    ::Platform::Util::Time_STM32F7 time;

    HardwareSTM32F7Discovery():
      ledGreenPin({9, 2}),
      ledOrangePin({0, 15}),
      ledRedPin({0, 8}),
      ledBluePin({1, 15}),
      btnUserPin({9, 3})
    {

      ledGreen  = new GPIO("ledGreen",  ledGreenPin);
      ledOrange = new GPIO("ledOrange", ledOrangePin);
      ledRed    = new GPIO("ledRed",    ledRedPin);
      ledBlue   = new GPIO("ledBlue",   ledBluePin);
      btnUser   = new GPIO("btnUserPin",  btnUserPin);
      
      ledGreen->SetMode       (GPIO::Mode::OUTPUT);
      ledOrange->SetMode      (GPIO::Mode::OUTPUT);
      ledRed->SetMode         (GPIO::Mode::OUTPUT);
      ledBlue->SetMode        (GPIO::Mode::OUTPUT);
      //btnUser->SetMode        (GPIO::Mode::INPUT);
    }
};
