#pragma once

#include <Util/EmitterReceiver.h>
#include <OS/Util/Tasks.h>
#include <HW/Util/Trace.h>

class BlinkyTask : public ::Platform::Util::SysTask
{ 
  public:
    ::Platform::Util::DataEmitter<bool>   outputState;

    BlinkyTask() :
      SysTask("BlinkyTask")
    {}

  protected:
    virtual void OnRun(){
      bool state = false;
      float counter = 0;
      for(;;){
        Platform::Semihost::Out() << "Semihosting - " << counter << std::endl;
        vTaskDelay(100);
        outputState.Emit(state);
        state = !state;
        counter += 0.1f;
      }
    }
};
