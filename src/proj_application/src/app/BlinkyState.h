#pragma once

#include <Util/StateMachine.h>
#include <Util/EmitterReceiver.h>
#include <Util/Timing.h>

struct BlinkyStateShared
{
  BlinkyStateShared():
    trigger(false)
  {}

  ::Platform::Util::DataReceiverStore<bool>                 trigger;
  ::Platform::Util::DataEmitter<bool>                       output;

  ::Platform::Util::StateMachine<BlinkyStateShared>::State* active;
  ::Platform::Util::StateMachine<BlinkyStateShared>::State* inactive;
};

class BlinkyStateBase : public ::Platform::Util::StateMachine<BlinkyStateShared>
{};

class BlinkyStateBaseState : public ::Platform::Util::StateMachine<BlinkyStateShared>::State
{
  protected:
	  typedef ::Platform::Util::StateMachine<BlinkyStateShared>::State Inherited;
};

class BlinkyStateActive : public BlinkyStateBaseState
{
  public:
		virtual void OnEnter(BlinkyStateShared* sData){
			Inherited::OnEnter(sData);
      sData->output.Emit(true);
      m_timer.Start(1000);
		}

    virtual Inherited* OnProcess(){
      Inherited* nextState = this;

      if(m_timer.Expired())
        nextState = GetSData()->inactive;

      return nextState; 
    }

		virtual void OnExit(){}

  private:
    ::Platform::Timing::Timer m_timer;
};

class BlinkyStateInActive : public BlinkyStateBaseState
{
  public:
		virtual void OnEnter(BlinkyStateShared* sData){
			Inherited::OnEnter(sData);
      sData->output.Emit(false);
		}

    virtual Inherited* OnProcess(){
      Inherited* nextState = this;
      if(*GetSData()->trigger == true)
        nextState = GetSData()->active;
      return nextState; 
    }
    
		virtual void OnExit(){}
};

class BlinkyState : public BlinkyStateBase
{
  public:
    BlinkyState()
    {
      shared.active = new BlinkyStateActive;
      shared.inactive = new BlinkyStateInActive;
      
      SetSharedData(&shared);
      SetInitialState(shared.inactive);
    }

    BlinkyStateShared   shared;
};
