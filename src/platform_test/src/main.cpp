
#include <gmock/gmock.h>


TEST(Basic_Basic, Equals) {
	// This test is named "Negative", and belongs to the "FactorialTest"
	// test case.
	EXPECT_EQ(1, 1);
	EXPECT_NE(1, 2);
	EXPECT_NEAR(0.1f, 0.3f, 0.5f);

}



#if !defined(_WIN32) && (defined(__unix__) || defined(__unix) || (defined(__APPLE__) && defined(__MACH__)))

#else

#include "windows.h"
TEST(Basic_Basic, Delay) {
	// This test is named "Negative", and belongs to the "FactorialTest"
	// test case.
	
	Sleep(1000);
}

#endif


#include <stdio.h>

int main(int argc, char** argv) {
  // The following line must be executed to initialize Google Mock
  // (and Google Test) before running the tests.
  ::testing::InitGoogleMock(&argc, argv);
  bool result = RUN_ALL_TESTS();
  //getchar();
  return result;
}
