#include <gmock/gmock.h>
#include <iostream>

#include <Util/EmitterReceiver.h>

using namespace ::Platform::Util;

#define VERBOSE(__v)


TEST(EmitterReceiver, Link){
  DataEmitter<int> emitter;
  DataReceiverStore<int> receiver(0);
  emitter.Link(&receiver);
}


TEST(EmitterReceiver, Emit){
  DataEmitter<int> emitter;
  DataReceiverStore<int> receiver(0);
  emitter.Link(&receiver);
  emitter.Emit(1337);

  EXPECT_EQ(*receiver, 1337);
}

TEST(EmitterReceiver, LinkNull){
  DataEmitter<int> emitter;
  try{
    emitter.Link(NULL);
    FAIL() << "Should've thrown NULL exception" << std::endl;
  }catch(...){
  }
}