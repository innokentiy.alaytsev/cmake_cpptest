#include <gtest/gtest.h>
#include <Util/SmartBuffer.h>

using namespace ::Platform::Util;

struct DataStruct
{
  float f;
  int i;
};

TEST(Platform_Util_SmartBuffer, SmartArrayBuffer_instanceiate)
{
  SmartArrayBuffer<int>               sb1;
  SmartArrayBuffer<int, 32>           sb2;
  SmartArrayBuffer<char, 32>          sb3c;
  SmartArrayBuffer<float, 32>         sb3f;
  SmartArrayBuffer<DataStruct, 32>    sb4;
}


TEST(Platform_Util_SmartBuffer, SmartArrayBuffer_usage)
{
  const int size = 16;
  SmartArrayBuffer<int, size>               sb;
  EXPECT_EQ(sb.GetDataCount(), 0);

  sb.Reset();
  EXPECT_EQ(sb.GetDataCount(), 0);

  for(int i = 0; i < size; i++)
  {
    sb.Insert(i);
  }
    
  EXPECT_EQ(sb.GetDataCount(), size);

  int* data = sb.GetData();
  for(int i = 0; i < size; i++)
  {
    if(data[i] != i)
    {
      FAIL() << "Unexpected Value at pos " << i << ": " << data[i] << std::endl;
    }
  }
}
