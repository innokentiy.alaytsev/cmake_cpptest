#include <gmock/gmock.h>
#include <iostream>

#include <Util/PreventDelete.h>

using namespace ::Platform::Util;

class TestObject : PreventDelete
{
};


TEST(Platform_Util_PreventDelete, PreventDelete)
{
  try{
    /*
    PreventDelete* prevDel = new PreventDelete;
    delete prevDel;
    */
    DOEXCEPT("Can't catch Exception currently");
    FAIL() << "Should have thrown" << std::endl;
  }catch(::Platform::Exception& ex){
    std::cout << ex.what() << std::endl;
  }catch(...){
    FAIL() << "Unexpected Error" << std::endl;
  }
}
