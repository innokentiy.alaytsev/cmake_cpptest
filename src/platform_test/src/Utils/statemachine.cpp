#include <iostream>

#include <gmock/gmock.h>
#include <Util/StateMachine.h>

using ::testing::AtLeast;
using namespace ::Platform::Util;

//#define VERBOSE(__v)	__v
#define VERBOSE(__v)

//#define INFO(__v)	__v
#define INFO(__v)

class TestStateBase;


class CallNotifyer
{
	public:
		virtual void Enter(void* ptr){
			INFO(std::cout << "CallNotifyer::OnEnter " << std::hex << ptr << std::endl);
		}
		virtual void Exit(void*ptr){
			INFO(std::cout << "CallNotifyer::OnExit " << std::endl);
		}
};

struct TestStateSharedData
{
		TestStateSharedData(){}

		StateMachine<TestStateSharedData>::State*			hello;
		StateMachine<TestStateSharedData>::State*			world;

		CallNotifyer*						cNotify;
};


class TestStateBase : public StateMachine<TestStateSharedData>::State
{
	typedef StateMachine<TestStateSharedData>::State Inherited;

	public:
		virtual void OnEnter(TestStateSharedData* sData){
			Inherited::OnEnter(sData);
			VERBOSE(std::cout << "TestStateBase::OnEnter" << std::endl);
			if(GetSData() != NULL && GetSData()->cNotify != NULL)
				GetSData()->cNotify->Enter((void*)this);
		}

		virtual void OnExit(){
			if(GetSData() != NULL && GetSData()->cNotify != NULL)
				GetSData()->cNotify->Exit((void*)this);
			VERBOSE(std::cout << "TestStateBase::OnExit" << std::endl);
		}
};




class TestStateHello : public TestStateBase
{
	typedef StateMachine<TestStateSharedData>::State Inherited;

	public:
		virtual Inherited* OnProcess(){
			VERBOSE(std::cout << "TestStateWorld::OnProcess" << std::endl);
			return GetSData()->world;
		}
};


class TestStateWorld : public TestStateBase
{
	typedef StateMachine<TestStateSharedData>::State Inherited;

	public:
		virtual Inherited* OnProcess(){
			VERBOSE(std::cout << "TestStateWorld::OnProcess" << std::endl);
			return GetSData()->hello;
		}
};


class TestState : public StateMachine<TestStateSharedData>
{
	typedef StateMachine<TestStateSharedData> Inherited;
 
 public:
  void Init() {
			VERBOSE(std::cout << "TestState::SetUp" << std::endl);
			sData.cNotify = NULL;
			sData.hello = new TestStateHello;
			sData.world = new TestStateWorld;

			SetInitialState(sData.hello);
			SetSharedData(&sData);
			VERBOSE(std::cout << "TestState::SetUp ... done" << std::endl);
	}

	virtual ~TestState(){
		VERBOSE(std::cout << "TestState::Destruct" << std::endl);
		delete sData.hello;
		delete sData.world;
	}

	TestStateSharedData sData;
};


TEST(TestStates, Execution){
	TestState tStateMachine;
	tStateMachine.Init();
	EXPECT_EQ(tStateMachine.GetCurrentState(), nullptr);
	tStateMachine.Execute();
	EXPECT_NE(tStateMachine.GetCurrentState(), nullptr);
}


TEST(TestStates, NullCheckSharedData){
	TestState tStateMachine;
	tStateMachine.Init();
	tStateMachine.SetSharedData(nullptr);
	EXPECT_EQ(tStateMachine.GetCurrentState(), nullptr);
	
	try{
		tStateMachine.Execute();
		FAIL();
	}catch(::Platform::Exception& ex){
		VERBOSE(std::out << "Expected Exception: " << ex.what() << std::endl);
	}catch(...){
		FAIL() << "Unexpected Exception";
	}
}


TEST(TestStates, NullCheckNextState){
	TestState tStateMachine;
	tStateMachine.Init();
	tStateMachine.SetInitialState(nullptr);
	EXPECT_EQ(tStateMachine.GetCurrentState(), nullptr);

	try{
		tStateMachine.Execute();
		FAIL();
	}catch(::Platform::Exception& ex){
		VERBOSE(std::out << "Expected Exception: " << ex.what() << std::endl);
	}catch(...){
		FAIL() << "Unexpected Exception";
	}
}


TEST(TestStates, transition_general){
	CallNotifyer cNotify;
	TestState tStateMachine;
	tStateMachine.Init();
	tStateMachine.sData.cNotify = &cNotify;
	EXPECT_EQ((unsigned long)tStateMachine.GetCurrentState(), 0);

	INFO(std::cout << "Step" << std::endl);
	tStateMachine.Execute();
	EXPECT_EQ((unsigned long)tStateMachine.GetCurrentState(), (unsigned long)tStateMachine.sData.hello);

	INFO(std::cout << "Step" << std::endl);
	tStateMachine.Execute();
	EXPECT_EQ((unsigned long)tStateMachine.GetCurrentState(), (unsigned long)tStateMachine.sData.world);
}



class CallMock : public CallNotifyer
{
	public:
		MOCK_METHOD1(Enter, void(void*ptr));
		MOCK_METHOD1(Exit, void(void*ptr));
};

TEST(TestStates, transition_deep){
	CallMock cMock;
	TestState tStateMachine;
	tStateMachine.Init();
	tStateMachine.sData.cNotify = &cMock;

	EXPECT_CALL(cMock, Enter((void*)tStateMachine.sData.hello)).Times(2);
	EXPECT_CALL(cMock, Exit((void*)tStateMachine.sData.hello)).Times(2);

	EXPECT_CALL(cMock, Enter((void*)tStateMachine.sData.world)).Times(2);
	EXPECT_CALL(cMock, Exit((void*)tStateMachine.sData.world)).Times(2);

	EXPECT_EQ((unsigned long)tStateMachine.GetCurrentState(), 0);

	INFO(std::cout << "Step" << std::endl);
	tStateMachine.Execute();
	EXPECT_EQ((unsigned long)tStateMachine.GetCurrentState(), (unsigned long)tStateMachine.sData.hello);

	INFO(std::cout << "Step" << std::endl);
	tStateMachine.Execute();
	EXPECT_EQ((unsigned long)tStateMachine.GetCurrentState(), (unsigned long)tStateMachine.sData.world);

	INFO(std::cout << "Step" << std::endl);
	tStateMachine.Execute();
	EXPECT_EQ((unsigned long)tStateMachine.GetCurrentState(), (unsigned long)tStateMachine.sData.hello);

	INFO(std::cout << "Step" << std::endl);
	tStateMachine.Execute();
	EXPECT_EQ((unsigned long)tStateMachine.GetCurrentState(), (unsigned long)tStateMachine.sData.world);
}
