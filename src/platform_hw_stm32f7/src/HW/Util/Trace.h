#pragma once

#include <ostream>


int trace_printf(const char* format, ...);
namespace Platform
{
  class Semihost
  {
    public:
      class File
      {

        public: 
          enum Mode
          {
            Mode_Read,
            Mode_ReadBinary,
            Mode_ReadUpdate,
            Mode_ReadBinaryUpdate,
            Mode_Write,
            Mode_WriteBinary,
            Mode_WriteUpdate,
            Mode_WriteBinaryUpdate,
            Mode_Append,
            Mode_AppendBinary,
            Mode_AppendUpdate,
            Mode_AppendBinaryUpdate
          };
          File(const File&) = delete;
          File(std::string name, Mode mode);

          virtual ~File(){
            Close();
          }

          virtual bool Open();
          virtual bool IsOpen();
          virtual void Close();

          virtual int Read(void* buffer, int length);
          virtual int Write(void* buffer, int length);
          virtual void Seek(size_t pos);

        private:
          size_t    m_pos;
          int m_file_handle;
          std::string m_fileName;
          Mode m_mode;
      };

      static void ExitOK();
      static void SysExecute(const char* call, unsigned int length);

      static File& TTY();
      static std::ostream& Out();
  };
}
