#include "UART_stm32f7.h"
#include <stm32f7xx.h>

namespace Platform {
  namespace Driver {
    bool UART_STM32F7::Send(char c){
      USART1->TDR = c;
      for(;(USART1->ISR & USART_ISR_TXE) != 0;);
    };

    void UART_STM32F7::PortEnable(unsigned int id){
      RCC->APB1ENR |= RCC_APB1ENR_USART2EN;
      RCC->APB1ENR |= RCC_APB1ENR_USART3EN;
      RCC->APB1ENR |= RCC_APB1ENR_UART4EN;
      RCC->APB1ENR |= RCC_APB1ENR_UART5EN;
      RCC->APB2ENR |= RCC_APB2ENR_USART1EN;
      RCC->APB2ENR |= RCC_APB2ENR_USART6EN;

      USART1->CR1 |= USART_CR1_UE;
    }
  }
}
