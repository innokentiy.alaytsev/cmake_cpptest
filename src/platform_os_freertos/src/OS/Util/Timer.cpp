#include "Timer.h"


namespace Platform
{
  namespace Util
  {

    void TimerCallback(TimerHandle_t args){
      SysTimer* timer = ( SysTimer* ) pvTimerGetTimerID( args );
      timer->Callback();
    }

    SysTimer::SysTimer(
          const std::string& name, 
          float interval, 
          bool autoreload,
          float delay) :
      NamedObject(name),
      m_interval(interval)
    {
      m_handle = xTimerCreate("SysTimer",
        (TickType_t)( configTICK_RATE_HZ * m_interval ),
        autoreload,
        ( void * ) this,
        TimerCallback
      );

      xTimerStart(m_handle, (TickType_t)( configTICK_RATE_HZ * delay));
    }

    void ExecutableTimer::OnCallback(){
      for(typename List<Executable*>::Iterator iter = m_executables.Begin();
          iter != m_executables.End();
          iter++){
        iter->Execute();
      }
    }

  }
}

