
#include <stdio.h>
#include <stdarg.h>
#include <Util/Error.h>
#include "Trace.h"


// http://www.keil.com/support/man/docs/armcc/armcc_pge1358787046598.htm

// Semihosting operations.
struct OperationNumber
{
  enum Operation {
    // Regular operations
    SEMIHOSTING_EnterSVC = 0x17,
    SEMIHOSTING_ReportException = 0x18,
    SEMIHOSTING_SYS_CLOSE = 0x02,
    SEMIHOSTING_SYS_CLOCK = 0x10,
    SEMIHOSTING_SYS_ELAPSED = 0x30,
    SEMIHOSTING_SYS_ERRNO = 0x13,
    SEMIHOSTING_SYS_FLEN = 0x0C,
    SEMIHOSTING_SYS_GET_CMDLINE = 0x15,
    SEMIHOSTING_SYS_HEAPINFO = 0x16,
    SEMIHOSTING_SYS_ISERROR = 0x08,
    SEMIHOSTING_SYS_ISTTY = 0x09,
    SEMIHOSTING_SYS_OPEN = 0x01,
    SEMIHOSTING_SYS_READ = 0x06,
    SEMIHOSTING_SYS_READC = 0x07,
    SEMIHOSTING_SYS_REMOVE = 0x0E,
    SEMIHOSTING_SYS_RENAME = 0x0F,
    SEMIHOSTING_SYS_SEEK = 0x0A,
    SEMIHOSTING_SYS_SYSTEM = 0x12,
    SEMIHOSTING_SYS_TICKFREQ = 0x31,
    SEMIHOSTING_SYS_TIME = 0x11,
    SEMIHOSTING_SYS_TMPNAM = 0x0D,
    SEMIHOSTING_SYS_WRITE = 0x05,
    SEMIHOSTING_SYS_WRITEC = 0x03,
    SEMIHOSTING_SYS_WRITE0 = 0x04,

    // Codes returned by SEMIHOSTING_ReportException
    ADP_Stopped_ApplicationExit = ((2 << 16) + 38),
    ADP_Stopped_RunTimeError = ((2 << 16) + 35),
  };
};


#if defined(__ARM_ARCH_7M__)     \
    || defined(__ARM_ARCH_7EM__) \
    || defined(__ARM_ARCH_6M__)
#define AngelSWIInsn                    "bkpt"
#define AngelSWIAsm                     bkpt
#else
#define AngelSWIInsn                    "swi"
#define AngelSWIAsm                     swi
#endif
#define AngelSWI                        0xAB
static inline int
call_host (unsigned int reason, void* arg)
{
  int value;
  asm (

      " mov r0, %[rsn]  \n"
      " mov r1, %[arg]  \n"
#if defined(OS_DEBUG_SEMIHOSTING_FAULTS)
      " " AngelSWITestFault " \n"
#else
      " " AngelSWIInsn " %[swi] \n"
#endif
      " mov %[val], r0"

      : [val] "=r" (value) /* Outputs */
      : [rsn] "r" (reason), [arg] "r" (arg), [swi] "i" (AngelSWI) /* Inputs */
      : "r0", "r1", "r2", "r3", "ip", "lr", "memory", "cc"
      // Clobbers r0 and r1, and lr if in supervisor mode
  );

  // Accordingly to page 13-77 of ARM DUI 0040D other registers
  // can also be clobbered. Some memory positions may also be
  // changed by a system call, so they should not be kept in
  // registers. Note: we are assuming the manual is right and
  // Angel is respecting the APCS.
  return value;
}





#include <Util/SmartBuffer.h>

class StreamBuffer : public std::streambuf
{
  ::Platform::Util::SmartArrayBuffer<char, 128> _buffer;

  protected:
	/* central output function
	 * - print characters in uppercase mode
	 */
	virtual int_type overflow (int_type c) {
    char ch = static_cast<char>(c);
    _buffer.Insert(ch);
    if(ch == '\n' || ch == 0 || _buffer.IsFull())
    {
      Platform::Semihost::TTY().Write(_buffer.GetData(), _buffer.GetDataCount());
      _buffer.Reset();
    }
    return c;
	}
};


namespace Platform 
{
  Semihost::File::File(std::string name, Mode mode) :
    m_pos(0),
    m_fileName(name),
    m_file_handle(-1),
    m_mode(mode)
  {}

  bool Semihost::File::Open()
  {
    if(m_file_handle > 0){
      return true;
    }

    void* block[3];
    block[0] = (void*)m_fileName.c_str();
    block[1] = (void*)m_mode;
    block[2] = (void*)m_fileName.length();

    m_file_handle = call_host(OperationNumber::SEMIHOSTING_SYS_OPEN, block);
    
    if(m_file_handle <= 0){
      DOEXCEPT("Couldn't open File");
      return false;
    }
    return true;
  }

  bool Semihost::File::IsOpen()
  {
    return m_file_handle > 0;
  }

  void Semihost::File::Close()
  {
    if(m_file_handle <= 0){
      return;
    }

    void* block[3];
    block[0] = (void*)m_file_handle;

    m_file_handle = call_host(OperationNumber::SEMIHOSTING_SYS_CLOSE, block);
  }

  void Semihost::File::Seek(size_t pos){
    if(m_file_handle <= 0){
      DOEXCEPT("File not open");
    }
    m_pos = pos;

    void* block[3];
    block[0] = (void*)m_file_handle;
    block[1] = (void*)m_pos;

    call_host(OperationNumber::SEMIHOSTING_SYS_SEEK, block);
  }

  int Semihost::File::Read(void* buffer, int length)
  {
    if(m_file_handle <= 0){
      DOEXCEPT("File not open");
    }

    Seek(m_pos);

    void* block[3];
    block[0] = (void*)m_file_handle;
    block[1] = (void*)buffer;
    block[2] = (void*)length;

    int read = call_host(OperationNumber::SEMIHOSTING_SYS_READ, block);
    if(read == length){
      read = -1;
    }else{
      if(read == 0){
        read = length;
      }
      m_pos += read;
    }
    return read;
  }

  int Semihost::File::Write(void* buffer, int length)
  {
    if(m_file_handle <= 0){
      DOEXCEPT("File not open");
    }

    void* block[3];
    block[0] = (void*)m_file_handle;
    block[1] = (void*)buffer;
    block[2] = (void*)length;

    int written = call_host(OperationNumber::SEMIHOSTING_SYS_WRITE, block);
    if(written == length){
      written = -1;
    }else if(written == 0){
      written = length;
    }

    return written;
  }

struct ADP_Stopped {
  enum {
    UserInterruption 	= 0x20025,
    ApplicationExit 	= 0x20026
  };
};

  void Semihost::ExitOK(){
    unsigned int value = ADP_Stopped::ApplicationExit;
    call_host(OperationNumber::SEMIHOSTING_ReportException, &value);
  }

  void Semihost::SysExecute(const char* call, unsigned int length){
    void* block[3];
    block[0] = (void*)call;
    block[1] = (void*)length;
    block[2] = (void*)0;

    int written = call_host(OperationNumber::SEMIHOSTING_SYS_SYSTEM, block);
  }

  Semihost::File& Semihost::TTY()
  {
    static File output(":tt", File::Mode_Write);
    if(!output.IsOpen()) output.Open();
    return output;
  }

  std::ostream& Semihost::Out(){
    static StreamBuffer m_sb;
    static std::ostream m_out(&m_sb);
    return m_out;
  }
}


