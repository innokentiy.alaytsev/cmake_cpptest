# What it is
This is a basic Project for STM32F4 Platforms (and maybe sometime more) 

## It can
- Run Google Tests for non Hardwaredependeant Unit/Item-Tests
- Run CppTest on Hardware for more Hardwarespecific Unit/Item-Tests
- Use QEmu to run and Debug Project in an virtualized Environment, still on PC
- Build Binaryfile for targetdeployment


# How to start
For a List of Startcommands please have a look into [tasks.json](.vscode/tasks.json)


# What it looks like
## PackageDiagram
![PackageDiagram](https://gitlab.com/dominik.gausa/cmake_cpptest/raw/master/docs/images/PackageDiagram.png)

## Codecoverage
You can find the [codecoverage here](https://dominik.gausa.gitlab.io/cmake_cpptest/)

## Google Tests in VS2017
![Visual Studio 2017 Google-Tests](https://gitlab.com/dominik.gausa/cmake_cpptest/raw/master/docs/images/vs2017_gtest.jpg)

## Google Tests in VS-Code
![VS-Code Google-Tests](https://gitlab.com/dominik.gausa/cmake_cpptest/raw/master/docs/images/vscode_gtests.jpg)

## Running Project in QEmu in VS-Code
![VS-Code Run Project](https://gitlab.com/dominik.gausa/cmake_cpptest/raw/master/docs/images/vscode_proj_qemu.jpg)


# Sampleoutputs
## GoogleTest

```
[==========] Running 9 tests from 4 test cases.
[----------] Global test environment set-up.
[----------] 1 test from Basic_Basic
[ RUN      ] Basic_Basic.Equals
[       OK ] Basic_Basic.Equals (0 ms)
[----------] 1 test from Basic_Basic (2 ms total)

[----------] 3 tests from TestStates
[ RUN      ] TestStates.Execution
[       OK ] TestStates.Execution (0 ms)
[ RUN      ] TestStates.transition_general
[       OK ] TestStates.transition_general (0 ms)
[ RUN      ] TestStates.transition_deep
[       OK ] TestStates.transition_deep (0 ms)
[----------] 3 tests from TestStates (4 ms total)

[----------] 3 tests from Exceptions
[ RUN      ] Exceptions.1
[       OK ] Exceptions.1 (1 ms)
[ RUN      ] Exceptions.2
[       OK ] Exceptions.2 (0 ms)
[ RUN      ] Exceptions.3
[       OK ] Exceptions.3 (0 ms)
[----------] 3 tests from Exceptions (4 ms total)

[----------] 2 tests from EmitterReceiver
[ RUN      ] EmitterReceiver.Link
[       OK ] EmitterReceiver.Link (0 ms)
[ RUN      ] EmitterReceiver.Emit
[       OK ] EmitterReceiver.Emit (0 ms)
[----------] 2 tests from EmitterReceiver (3 ms total)

[----------] Global test environment tear-down
[==========] 9 tests from 4 test cases ran. (23 ms total)
[  PASSED  ] 9 tests.
```

## CppTest
```
GNU MCU Eclipse 64-bits QEMU v2.8.0-3 (D:\workspace\toolchain\QEMU\2.8.0-3-20180523-0703\bin\qemu-system-gnuarmeclipse.exe).
Board: 'STM32F4-Discovery' (ST Discovery kit for STM32F407/417 lines).
Device file: 'D:\workspace\toolchain\QEMU\2.8.0-3-20180523-0703\devices\STM32F40x-qemu.json'.
Device: 'STM32F407VG' (Cortex-M4 r0p0, MPU, 4 NVIC prio bits, 82 IRQs), Flash: 1024 kB, RAM: 128 kB.
Image: 'D:/workspace/cpp/cmake_cpptest/buildQemu/proj_test'.
Command line: 'test 1 2 3' (10 bytes).
Load 298304 bytes at 0x08000000-0x08048D3F.
Load  40488 bytes at 0x08048D40-0x08052B67.
Cortex-M4 r0p0 core initialised.
'/machine/mcu/stm32/RCC', address: 0x40023800, size: 0x0400
'/machine/mcu/stm32/FLASH', address: 0x40023C00, size: 0x0400
'/machine/mcu/stm32/PWR', address: 0x40007000, size: 0x0400
'/machine/mcu/stm32/SYSCFG', address: 0x40013800, size: 0x0400
'/machine/mcu/stm32/EXTI', address: 0x40013C00, size: 0x0400
'/machine/mcu/stm32/GPIOA', address: 0x40020000, size: 0x0400
'/machine/mcu/stm32/GPIOB', address: 0x40020400, size: 0x0400
'/machine/mcu/stm32/GPIOC', address: 0x40020800, size: 0x0400
'/machine/mcu/stm32/GPIOD', address: 0x40020C00, size: 0x0400
'/machine/mcu/stm32/GPIOE', address: 0x40021000, size: 0x0400
'/machine/mcu/stm32/GPIOF', address: 0x40021400, size: 0x0400
'/machine/mcu/stm32/GPIOG', address: 0x40021800, size: 0x0400
'/machine/mcu/stm32/GPIOH', address: 0x40021C00, size: 0x0400
'/machine/mcu/stm32/GPIOI', address: 0x40022000, size: 0x0400
'/machine/mcu/stm32/USART1', address: 0x40011000, size: 0x0400
'/machine/mcu/stm32/USART2', address: 0x40004400, size: 0x0400
'/machine/mcu/stm32/USART3', address: 0x40004800, size: 0x0400
'/machine/mcu/stm32/USART6', address: 0x40011400, size: 0x0400
'/peripheral/led:green' 8*10 @(258,218) active high '/machine/mcu/stm32/GPIOD',12
'/peripheral/led:orange' 8*10 @(287,246) active high '/machine/mcu/stm32/GPIOD',13
'/peripheral/led:red' 8*10 @(258,274) active high '/machine/mcu/stm32/GPIOD',14
'/peripheral/led:blue' 8*10 @(230,246) active high '/machine/mcu/stm32/GPIOD',15
GDB Server listening on: 'tcp::1234'...
Cortex-M4 r0p0 core reset.

[led:orange on]
World 1
[led:orange off]
World 102
[led:orange on]
. . .
World 1617
[led:orange off]
World 1718
[led:orange on]
World 1819
[led:orange off]
World 1920
TEST_TestState: 3/3, 100% correct in 0.000000 seconds
Total: 3 tests, 100% correct in 0.000000 seconds
```